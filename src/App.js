import logo from './logo.svg';
import './App.css';
import axios from 'axios'
import { useEffect, useState } from 'react';


function App() {

  const [films,setFilms] = useState([])
  const [useStateQueryString,setUseStateQueryString] = useState("")

  // let queryString  = "avengers"
  // let urlAPI = `http://localhost:4250/law-a1-api/get-films/${queryString}/`


  // useEffect(() => {

  //   const fetchAll = async () => {
  //     const resultFilms = await axios (
  //       urlAPI
  //     )

  //     setFilms(resultFilms.data.Search)

      
  //   }

  //   fetchAll()

  


  // }, [])

  const handleOnChangeMovie = (e) => {
    let updatedQueryString = e.target.value
    setUseStateQueryString(updatedQueryString)
    console.log(useStateQueryString)
  }

  const handleOnClickSearch = () => {
    let newUrlAPI = `http://localhost:4250/law-a1-api/get-films/${useStateQueryString}/`
    const fetchAllMovies = async () => {
      const newResultFilms = await axios (
        newUrlAPI
      )

      setFilms(newResultFilms.data.Search)

      
    }

    fetchAllMovies()

  }

  

  console.log('hasil fetch')
  console.log(films)




 
  return (
    <div className="App">
      <header className="App-header">

        
        
      <div class="wrap">

      <div class="search">
  
      <input type="text" class="searchTerm" placeholder="Type Your Movie" onChange={handleOnChangeMovie}/>
      <button type="submit" class="searchButton" onClick={handleOnClickSearch}>
        go!
     </button>
     </div>
 
      </div>
        

        

        {films.map((film)=>{
          let displayString = `${film.Title}; ${film.Year} ; ${film.imdbID}`

          return (

            <a
          className="App-link"
        
          rel="noopener noreferrer"
        >
          {displayString}
        </a>

          )
        })}
       
      </header>
    </div>
  );
}

export default App;
